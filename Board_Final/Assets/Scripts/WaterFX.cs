﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFX : MonoBehaviour
{
    public ParticleSystem RipplesFX;
    public ParticleSystem SplashFX;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            RipplesFX.gameObject.SetActive(true);
            RipplesFX.Play();
            SplashFX.Play();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            //print("dry");
            RipplesFX.gameObject.SetActive(false);
            RipplesFX.Stop();
            SplashFX.Stop();

        }
    }
}
