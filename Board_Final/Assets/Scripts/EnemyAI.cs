﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAI : MonoBehaviour
{
    int walkTarget = 0;
    public GameObject[] walkpoints;
    public Vector3 Orig;
    NavMeshAgent nav;
    bool waiting = false;
    // Start is called before the first frame update
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.SetDestination (walkpoints[walkTarget].transform.position);
        //walkpoints[walkTarget];

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, walkpoints[walkTarget].transform.position) < 1f && waiting == false)
        {
            waiting = true;
            Invoke("NextWalk", 6f);
        } 
    }

    void NextWalk()
    {
        waiting = false;
        walkTarget += 1;
        walkTarget = walkTarget % walkpoints.Length;
        nav.SetDestination(walkpoints[walkTarget].transform.position);
    }
}
